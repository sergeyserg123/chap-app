import { applyMiddleware, CombinedState, createStore, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import { DispatchType } from "../containers/Chat/types/dispatchType";
import { ReducerActions, RootReducer } from "./reducerTypes";
import { rootReducer } from './rootReducer';
import { rootSaga } from './rootSaga';
import { history } from './history';
import { routerMiddleware } from "connected-react-router";


const sagaMiddleware = createSagaMiddleware()

const composedEnhancers = composeWithDevTools(
    applyMiddleware(sagaMiddleware, routerMiddleware(history))
)

const store: Store<CombinedState<RootReducer>, ReducerActions> & {
    dispatch: DispatchType
} = createStore(rootReducer, composedEnhancers)

sagaMiddleware.run(rootSaga)

export default store