import { all } from "redux-saga/effects";
import { messageSagas } from "../containers/Chat/sagas";
import { loginSagas } from "../containers/LoginPage/sagas";
// import { userListSagas } from "../containers/UserList/sagas";

export function* rootSaga() {
    yield all([
        messageSagas(),
        loginSagas(),
        // userListSagas()
    ])
}