import { MessageState } from "../containers/Chat/types/messageState";
import { UserListState } from "../containers/UserList/types/userListState";
import { LoginState } from "../containers/LoginPage/types/loginState";
import { MessageAction } from "../containers/Chat/types/messageAction";
import { LoginAction } from "../containers/LoginPage/types/loginAction";
import { UserListAction } from "../containers/UserList/types/userListAction";
import { Reducer } from "react";
import { RouterState } from "connected-react-router";

type RootReducer = {
    messageReducer: MessageState
    // userListReducer: UserListState
    loginReducer: LoginState
    router: Reducer<RouterState>
}

type ReducerActions = MessageAction | LoginAction | UserListAction 