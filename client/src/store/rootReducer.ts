import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import { messageReducer } from '../containers/Chat/reducer';
import { loginReducer } from '../containers/LoginPage/reducer';
// import { userListReducer } from '../containers/UserList/reducer';
import { history } from './history';


export const rootReducer = combineReducers({
    messageReducer,
    loginReducer,
    // userListReducer,
    router: connectRouter(history)
})