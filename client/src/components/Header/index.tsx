import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { formatDate } from '../../helpers/dateHelper';
import { roles } from '../../models/enums/roles';
import { UserData } from '../../models/users/userData';
import './styles.scss';

type HeaderProps = {
    participants?: number
    messages?: number
    lastMessageTime?: string
    userData?: UserData
    isAuthorized?: boolean
}

export const Header: FunctionComponent<HeaderProps> = ({ participants, messages, lastMessageTime, userData }) => {
    return (
        <header>
            <div className="chat-details-area">
                <div className="chat-name">My Chat</div>
                {participants && <div className="chat-participants">{participants} participants</div>}
                {messages && <div className="chat-messages">{messages} messages</div>}           
            </div>
            
            <div className="time-area">
                {(userData && userData.role === roles.admin) && <Link to="/user-list">user list</Link>}
                <span> {lastMessageTime? `Last message at ${formatDate(lastMessageTime)}` : ''}</span>
            </div>
        </header>
    );
}