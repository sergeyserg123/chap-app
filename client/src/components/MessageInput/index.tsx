import React, { ChangeEvent, FunctionComponent, useState } from 'react';
import './styles.scss';

type MessageInputProps = {
    onCreateMessage: (messageText: string) => void;
}

export const MessageInput: FunctionComponent<MessageInputProps> = ({ 
    onCreateMessage 
}) => {
    const [messageText, setMessageText] = useState('');

    const onTypeKeyboard = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const { value } = e.target;
        setMessageText(value);
    };

    const createNewMessage = (): void => {
        if (messageText.length <= 0) {
            return;
        }
        onCreateMessage(messageText);
        setMessageText('');
    };

    return (
        <div className="message-input-container">
            <textarea onChange={onTypeKeyboard} placeholder="Type your message here..." value={messageText}></textarea>
            <button onClick={createNewMessage}>Send</button>
        </div>
    )
}