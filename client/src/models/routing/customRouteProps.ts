import { RouteProps } from "react-router-dom";
import { LoginAction } from "../../containers/LoginPage/types/loginAction";
import { UserData } from "../users/userData";

export interface CustomRouteProps extends RouteProps {
  loadUser?: () => LoginAction
  isAuthorized?: boolean
  userData?: UserData
  component?: any
}