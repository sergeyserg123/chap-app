export interface NewUser {
    user: string;
    email: string;
    password: string;
}