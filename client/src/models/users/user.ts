export interface User {
    userId: string;
    avatar: string;
    email: string;
    user: string;
    role: string;
    createdAt: string;
    editedAt: string;
}