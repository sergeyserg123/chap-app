import { NewUser } from "../models/users/newUser"
import { User } from "../models/users/user"
import { addUserToList, deleteUserFromList, editUserInList, fetchUser, fetchUsers } from "./domainRequest/userListRequest"

export const add = async (user: NewUser) => {
    return await addUserToList(user)
}

export const fetch = async (id: string) => {
    return await fetchUser(id)
}

export const fetchAll = async () => {
    return await fetchUsers()
}

export const edit = async (user: User) => {
    const { userId: id } = user
    return await editUserInList(id, user)
}

export const deleteReq = async (id: string) => {
    return await deleteUserFromList(id)
}
