const apiUrl = 'http://localhost:3050/api';

export const getAll = async (entity: string) => {
    return await makeRequest(`${apiUrl}/${entity}`, 'GET');
}

export const get = async (entity: string, id: string | null) => {
    return await makeRequest(`${apiUrl}/${entity}/${id}`, 'GET');
}

export const postWithParam = async <T>(entity: string, id: string | null) => {
    return await makeRequest(`${apiUrl}/${entity}/${id}`, 'POST');
}

export const post = async <T>(entity: string, body: T) => {
    return await makeRequest(`${apiUrl}/${entity}`, 'POST', body);
}

export const put = async <T>(entity: string, id: string, body: T) => {
    return await makeRequest(`${apiUrl}/${entity}/${id}`, 'PUT', body);
}

export const deleteReq = async (entity: string, id: string) => {
    return await makeRequest(`${apiUrl}/${entity}/${id}`, 'DELETE');
}

const makeRequest = async <T>(path: string, method: string, body?: T) => {
    try {
        const url = path;
        const res = await fetch(url, {
            method: method,
            body: body ? JSON.stringify(body) : undefined ,
            headers: { "Accept": "application/json", 
            "Content-type": "application/json"}
        });

        const dataObj = await res.json();

        if(res.ok) {
            return dataObj;
        }

        alert(`${dataObj.message}`);
        return dataObj;
    } catch (err) {
        console.error(err);
    }
}