import { Message } from "../../models/messages/message";
import { deleteReq, get, getAll, post, put } from "../requestHelper";

const entity: string = 'messages';

export const getMessages = async () => {
    return await getAll(entity);
};

export const getMessageById = async (id: string) => {
    return await get(entity, id);
};

export const createMessage = async (newMessage: Message) => {
    return await post<Message>(entity, newMessage);
};

export const updateMessage = async (id: string, message: Message) => {
    return await put<Message>(entity, id, message);
};

export const deleteMessage = async (id: string) => {
    return await deleteReq(entity, id);
};