import { LoginUser } from "../../models/users/loginUser";
import { NewUser } from "../../models/users/newUser";
import { post, postWithParam } from "../requestHelper";

const entity: string = 'users'
const authEntity: string = 'auth/login'

export const loginUser = async (user: LoginUser) => {
    return await post(authEntity, user)
}

export const registerUser = async (newUser: NewUser) => {
    return await post(entity, newUser)
}

export const loadCurrentUser = async (id: string | null) => {
    return await postWithParam(entity, id)
}