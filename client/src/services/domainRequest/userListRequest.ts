import { NewUser } from "../../models/users/newUser"
import { User } from "../../models/users/user"
import { deleteReq, get, getAll, post, put } from "../requestHelper"

const entity: string = 'users'

export const addUserToList = async (user: NewUser) => {
    return await post(entity, user)
}

export const fetchUser = async (id: string) => {
    return await get(entity, id)
}

export const fetchUsers = async () => {
    return await getAll(entity)
}

export const editUserInList = async (id: string, user: User) => {
    return await put(entity, id, user)
}

export const deleteUserFromList = async (id: string) => {
    return await deleteReq(entity, id)
}