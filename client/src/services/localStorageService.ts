import { User } from "../models/users/user";

export const setUserId = (id: string, role: string) => {
    localStorage.setItem('userId', id);
    localStorage.setItem('role', role);
};

export const getUserData = () : User => {
    const userId = localStorage.getItem('userId');
    const avatar = localStorage.getItem('avatar');
    const user = localStorage.getItem('user');
    const userObj = {
        userId,
        avatar,
        user
    };
    return userObj as User;
}

export const getUserId = (): string | null => localStorage.getItem('userId');