import { LoginUser } from "../models/users/loginUser";
import { NewUser } from "../models/users/newUser";
import { UserData } from "../models/users/userData";
import { loginUser, registerUser, loadCurrentUser } from "./domainRequest/accountRequest";
import * as localStorageService from "./localStorageService";


export const login = async (user: LoginUser): Promise<UserData> => {
    return await loginUser(user)
}

export const register = async (newUser: NewUser) => {
    return await registerUser(newUser)
}

export const loadUser = async () => {
    const userId = localStorageService.getUserId()
    return await loadCurrentUser(userId)
}