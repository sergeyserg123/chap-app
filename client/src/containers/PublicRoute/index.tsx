import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { FunctionComponent } from 'react';
import { CustomRouteProps } from '../../models/routing/customRouteProps';
import { RootReducer } from '../../store/reducerTypes';

const PublicRoute: FunctionComponent<CustomRouteProps> = ({ component: Component, isAuthorized, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      : <Component {...props} />)}
  />
);

const mapStateToProps = (state: RootReducer) => ({
  isAuthorized: state.loginReducer.isAuthorized
});

export default connect(mapStateToProps)(PublicRoute);