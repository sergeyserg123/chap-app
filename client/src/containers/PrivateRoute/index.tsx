import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { RootReducer } from '../../store/reducerTypes';
import { FunctionComponent } from 'react';
import { CustomRouteProps } from '../../models/routing/customRouteProps';

const PrivateRoute: FunctionComponent<CustomRouteProps> = ({ 
  component: Component, 
  isAuthorized, 
  ...rest 
}) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

const mapStateToProps = (state: RootReducer) => ({
  isAuthorized: state.loginReducer.isAuthorized
});

export default connect(mapStateToProps)(PrivateRoute);