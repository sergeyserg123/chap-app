import React from "react";
import { connect } from "react-redux";
import { Button, Form, Grid, Header, Segment } from "semantic-ui-react";
import { getDate } from "../../helpers/dateHelper";
import { Message } from "../../models/messages/message";
import { RootReducer } from "../../store/reducerTypes";
import { updateMessage, fetchMessage } from "../Chat/actions";
import { MessageEditorProps, MessageEditorState } from "./MessageEditor";

class MessageEditor extends React.Component<MessageEditorProps, MessageEditorState> {
    state: Readonly<MessageEditorState> = {
        message: null,
        editText: ''
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchMessage(this.props.match.params.id)
        } 
    }

    static getDerivedStateFromProps(nextProps: MessageEditorProps, prevState: MessageEditorState) {
        if (nextProps.editable?.id !== prevState.message?.id) {
            return {
                message: nextProps.editable,
                editText: nextProps.editable.text
            };
        } else {
            return null;
        }
    }

    onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const { value } = e.target
        this.setState({ editText: value })
    }

    onSubmit = () => {
        const { editText, message } = this.state
        if (editText.length > 0) {
            const updatedMessage = {
                ...message,
                text: editText,
                editedAt: getDate()
            } as Message
            this.props.updateMessage(updatedMessage)
            this.props.history.push('/')
        }
    }
    
    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        Edit your message
                    </Header>
                    <Form size='large'>
                        <Segment stacked>
                            <Form.TextArea value={this.state.editText} onChange={this.onChange} fluid="true" placeholder='Edit message...' />

                            <Button onClick={this.onSubmit} color='teal' size='small'>
                                Save
                            </Button>
                            <Button onClick={this.onSubmit} color='red' size='small'>
                                Cancel
                            </Button>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapStateToProps = (state: RootReducer) => ({
    location: state.router.location,
    editable: state.messageReducer.editableMessage
})

const mapDispatchToProps = {
    updateMessage,
    fetchMessage
}

// @ts-ignore: next line
export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);