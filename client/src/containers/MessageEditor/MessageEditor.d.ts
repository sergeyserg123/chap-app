import { RouteComponentProps } from "react-router-dom"
import { Message } from "../../models/messages/message"
import { MessaMatchParamsge } from "../../models/routing/matchParams"
import { MessageAction } from "../Chat/types/messageAction"


interface MessageEditorProps extends RouteComponentProps<MatchParams> {
    location: any
    editable: Message
    match: any
    history:  History<LocationState>
    updateMessage: (message: Message) => MessageAction
    fetchMessage: (id: string) => MessageAction
}

type MessageEditorState = {
    message: Message | null,
    editText: string
}

type MessageEditorAction = (message: Message) => MessageAction

declare class MessageEditor {
    static getDerivedStateFromProps(nextProps: MessageEditorProps, prevState: MessageEditorState)
}