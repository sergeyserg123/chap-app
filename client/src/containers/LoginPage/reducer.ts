import { LoginAction } from "./types/loginAction"
import * as actionTypes from "./actionTypes"
import { LoginState } from "./types/loginState"
import { UserData } from "../../models/users/userData"

const initialState: LoginState = {
    userData: {
        userId: '',
        user: '',
        role: '',
        email: ''   
    },
    isAuthorized: false,
    isLoginError: false,
    errorMessage: ''
}

export const loginReducer = (state: LoginState = initialState, action: LoginAction): LoginState => {
    switch (action.type) {
        case actionTypes.LOGIN_USER_SUCCESS:
            return {
                ...state,
                userData: action.payload as UserData,
                isAuthorized: true,
                errorMessage: '',
                isLoginError: false
            }

        case actionTypes.LOGIN_USER_FAILURE:
            return {
                ...state,
                isLoginError: true,
                isAuthorized: false,
                errorMessage: action.payload as string
            }
        
        case actionTypes.REGISTER_USER_SUCCESS:
            return {
                ...state,
                userData: action.payload as UserData
            }

        case actionTypes.LOAD_USER_SUCCESS:
            return {
                ...state,
                userData: action.payload as UserData,
                isLoginError: false,
                errorMessage: ''
            }

        case actionTypes.LOAD_USER_FAILURE:
            return {
                ...state,
                isLoginError: true,
                errorMessage: action.payload as string
            }
    }
    return state
}