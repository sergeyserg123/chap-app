import { LoginUser } from "../../models/users/loginUser"
import { NewUser } from "../../models/users/newUser"
import { LOAD_USER, LOGIN_USER, REGISTER_USER } from "./actionTypes"
import { LoginAction } from "./types/loginAction"

export const loginUser = (user: LoginUser): LoginAction => ({
    type: LOGIN_USER,
    payload: user
})

export const registerUser = (user: NewUser): LoginAction => ({
    type: REGISTER_USER,
    payload: user
})

export const loadUser = (): LoginAction => ({
    type: LOAD_USER,
    payload: null
})