import React from "react";
import { connect } from "react-redux";
import { Button, Form, Grid, Header, Segment } from "semantic-ui-react";
import { LoginUser } from "../../models/users/loginUser";
import { loginUser } from './actions';
import { LoginPageProps, LoginPageState } from './LoginPage';
import './styles.scss';

class LoginPage extends React.Component<LoginPageProps, LoginPageState> {
    state: Readonly<LoginPageState> = {
        email: '',
        password: ''
    }

    onChangeEmail = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ email: e.target.value })
    }

    onChangePassword = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ password: e.target.value })
    }

    onSubmit = () => {
        const { email, password } = this.state
        const loginData: LoginUser = {
            email,
            password
        }
        this.props.loginUser(loginData)
    }

    render(): JSX.Element {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        Log-in to your account
                    </Header>
                    <Form size='large'>
                        <Segment stacked>
                            <Form.Input onChange={this.onChangeEmail} fluid icon='user' iconPosition='left' placeholder='E-mail address' />
                            <Form.Input
                                fluid
                                onChange={this.onChangePassword}
                                icon='lock'
                                iconPosition='left'
                                placeholder='Password'
                                type='password'
                            />

                            <Button onClick={this.onSubmit} color='teal' fluid size='large'>
                                Login
                            </Button>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapDispatchToProps = {
    loginUser
}

export default connect(null, mapDispatchToProps)(LoginPage);