import { LoginUser } from "../../models/users/loginUser"
import { LoginAction } from "./types/loginAction"

type LoginPageProps = {
    loginUser: (user: LoginUser) => LoginAction
}

type LoginPageState = {
    email: string
    password: string
}

declare class LoginPage {
    onChangeEmail(e: React.ChangeEvent<HTMLInputElement>): void
    onChangePassword(e: React.ChangeEvent<HTMLInputElement>): void
    onSubmit(): void
    render(): JSX.Element
}