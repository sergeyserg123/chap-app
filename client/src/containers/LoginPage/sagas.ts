import { all, takeEvery, put } from "redux-saga/effects";
import { LoginUser } from "../../models/users/loginUser";
import { NewUser } from "../../models/users/newUser";
import { login, register, loadUser } from "../../services/accountService";
import { LOGIN_USER, REGISTER_USER, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE, LOAD_USER } from "./actionTypes";
import { LoginAction } from "./types/loginAction";
import * as localStorageService from '../../services/localStorageService'


export function* LoginUserSaga(action: LoginAction) {
    try {
        const userData = yield login(action.payload as LoginUser)
        if (userData) {
            const { id: userId, role } = userData;
            yield localStorageService.setUserId(userId, role)
            yield put({ type: LOGIN_USER_SUCCESS, payload: userData })
        }
    } catch (error) {
        // TODO: show errors
        yield put({ type: LOGIN_USER_FAILURE, payload: error.message })
    }
}

function* watchLoginUser() {
    yield takeEvery(LOGIN_USER, LoginUserSaga)
}

export function* LoadCurrentUser(action: LoginAction) {
    try {
        const userData = yield loadUser()
        if (!userData) {
            yield put({ type: LOGIN_USER_FAILURE, payload: "Couldn't load user data." })
            return;
        }
        yield put({ type: LOGIN_USER_SUCCESS, payload: userData })   
    } catch (error) {
        // TODO: show errors
        yield put({ type: LOGIN_USER_FAILURE, payload: error.message })
    }
}

function* watchLoadCurrentUser() {
    yield takeEvery(LOAD_USER, LoadCurrentUser)
}

// TODO register
export function* RegisterUser(action: LoginAction) {
    try {
        yield register(action.payload as NewUser)
    } catch (error) {

    }
}

function* watchRegisterUser() {
    yield takeEvery(REGISTER_USER, RegisterUser)
}

export function* loginSagas() {
    yield all([
        watchLoginUser(),
        watchRegisterUser(),
        watchLoadCurrentUser()
    ])
}