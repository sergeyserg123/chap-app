import { LoginUser } from "../../../models/users/loginUser";
import { UserData } from "../../../models/users/userData";

type LoginAction = {
    type: string
    payload: UserData | LoginUser | string | null
}