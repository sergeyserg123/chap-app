import { UserData } from "../../../models/users/userData";

type LoginState = {
    userData: UserData
    isLoginError: boolean
    errorMessage: string
    isAuthorized: boolean
}