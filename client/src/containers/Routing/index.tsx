import React, { FunctionComponent, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import LoginPage from '../LoginPage';
import Chat from '../Chat';
import { NotFound } from '../../components/NotFound';
import MessageEditor from '../MessageEditor';
import PrivateRoute from '../PrivateRoute';
import PublicRoute from '../PublicRoute';
// import UserList from '../UserList';
import UserEditor from '../UserEditor';
import { RootReducer } from '../../store/reducerTypes';
import { loadUser } from '../LoginPage/actions';
import { CustomRouteProps } from '../../models/routing/customRouteProps';

const Routing: FunctionComponent<CustomRouteProps> = ({ isAuthorized, userData, loadUser }) => {
  useEffect(() => {
    if (!isAuthorized) {    
      loadUser!();
    }
  });

  return (
        <Switch>
          <PublicRoute exact path="/login" component={LoginPage} />
          <PrivateRoute exact path="/" component={Chat} />
          <PrivateRoute exact path="/messages/:id" component={MessageEditor} />
          {/* <PrivateRoute exact path="/user-list" component={UserList} /> */}
          <PrivateRoute exact path="/user/:id" component={UserEditor} />
          <Route path="*" exact component={NotFound} />
        </Switch>
  );
};

const mapStateToProps = (state: RootReducer) => ({
  isAuthorized: state.loginReducer.isAuthorized,
  userData: state.loginReducer.userData
});


const mapDispatchToProps = {
  loadUser
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routing);