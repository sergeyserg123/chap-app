import React from 'react';
import { Header } from '../../components/Header';
import { MessageInput } from '../../components/MessageInput';
import { MessageList } from '../../components/MessageList';
import { Message } from '../../models/messages/message';
import { NewMessage } from '../../models/messages/newMessage';
import * as localStorageService from '../../services/localStorageService';
import { formatDate, getDate } from '../../helpers/dateHelper';
import { generateNewId } from '../../helpers/idGenerator';
import { ChatActions, PropTypes, StateTypes } from './Chat';
import { addMessage, loadMessages, deleteMessage } from './actions';
import { connect } from 'react-redux';
import { RootReducer } from '../../store/reducerTypes';

class Chat extends React.Component<PropTypes, StateTypes> {
    state: Readonly<StateTypes> = {
        messages: null,
        isLoading: false,
        ownAccounUserData: null
    };

    componentWillReceiveProps(nextProps: Readonly<PropTypes>): void {
        const { messages } = nextProps
        if (messages) {
            this.setState({ messages: messages, isLoading: false });
        }    
    }

    componentDidMount(): void {
        const ownAccounUserData = localStorageService.getUserData();
        this.setState({ isLoading: true, ownAccounUserData })
        this.props.loadMessages();
    }

    createNewMessage = (messageText: string): void => {
        const { ownAccounUserData } = this.state;
        const { userId, user, avatar } = ownAccounUserData!;
        const newMessage: NewMessage = {
            id: generateNewId(),
            userId,
            user,
            avatar,
            createdAt: getDate(),
            text: messageText
        };
        this.props.addMessage(newMessage as Message);
    }

    likeMessage = (message: Message): void => {
        const toggledLikeMessage: Message = {
            ...message,
            isLiked: !message.isLiked 
        }
        this.props.updateMessage(toggledLikeMessage);
    }


    // TODO: 
    setEditableMessage = (id: string): void => {     
        const { messages } = this.state;
        const editableMessage = messages?.find(message => message.id === id);
        this.props.history.push({
            pathname: `/messages/${id}`,
            state: {message: editableMessage} 
        },)
    }

    deleteMessage = (message: Message): void => {
        this.props.deleteMessage(message);
    }

    countParticipants() : number {
        const { messages } = this.state;
        const userIds = messages?.map(message => message.userId);
        return new Set(userIds).size;
    }

    countMessages() : number | undefined {
        const { messages } = this.state;
        return messages?.length;
    }

    getMessageTime(id: string): string {
        const { messages } = this.state;
        const message = messages!.find(m => m.id === id);
        const time = message!.createdAt;
        return formatDate(time);
    }

    getLastMessageTime(): string | undefined {
        const { messages } = this.state;
        if (messages != null) {
            const map = new Map<number, string>();
            const parsedArr = messages!.map(e => {
                const parsedDate: number = Date.parse(e.createdAt);
                map.set(parsedDate, e.createdAt);
                return parsedDate;
            });
            const maxDate = Math.max(...parsedArr)
            return map.get(maxDate);
        }
    }

    render() {
        return (
            <>
                <Header
                    isAuthorized={this.props.isAuthorized}
                    userData={this.props.userData}
                    participants={this.countParticipants()}
                    messages={this.countMessages()}
                    lastMessageTime={this.getLastMessageTime()}
                />
                <MessageList
                    currentUser={this.state.ownAccounUserData}
                    messagesList={this.state.messages}
                    isLoading={this.state.isLoading}
                    onLikeMessage={this.likeMessage}
                    onEditMessage={this.setEditableMessage}
                    onDeleteMessage={this.deleteMessage}
                />
                <MessageInput
                    onCreateMessage={this.createNewMessage}
                />
            </>
        );
    }
}

const mapStateToProps = (state: RootReducer) =>({
    messages: state.messageReducer.messages,
    isAuthorized: state.loginReducer.isAuthorized,
    userData: state.loginReducer.userData
})

const actions: ChatActions = {
    loadMessages,
    addMessage,
    deleteMessage
}

const mapDispatchToProps: ChatActions = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
