import { put, takeEvery, all } from 'redux-saga/effects';
import { ADD_MESSAGE, ADD_MESSAGE_FAILURE, ADD_MESSAGE_SUCCESS, DELETE_MESSAGE, DELETE_MESSAGE_FAILURE, DELETE_MESSAGE_SUCCESS, FETCH_MESSAGE, FETCH_MESSAGE_FAILURE, FETCH_MESSAGE_SUCCESS, LOAD_MESSAGES, LOAD_MESSAGES_FAILURE, LOAD_MESSAGES_SUCCESS, UPDATE_MESSAGE, UPDATE_MESSAGE_FAILURE, UPDATE_MESSAGE_SUCCESS } from './actionTypes';
import { create, deleteMess, update, getAll, get } from '../../services/messageService';
import { Message } from '../../models/messages/message';
import { MessageAction } from './types/messageAction';

export function* LoadMessages(action: MessageAction) {
    try {
        const messages: Message[] = yield getAll();
        yield put({type: LOAD_MESSAGES_SUCCESS, payload: {messages}})
    } catch (error) {
        // TODO: show error
        yield put({type: LOAD_MESSAGES_FAILURE})
    }
}

function* watchLoadMessages () {
    yield takeEvery(LOAD_MESSAGES, LoadMessages);
}

export function* FetchMessage(action: MessageAction) {
    try {
        const message: Message = yield get(action.payload!.editableMessageId!);
        if (message) {
            yield put({type: FETCH_MESSAGE_SUCCESS, payload: {message}})
        }
    } catch (error) {
        // TODO: show error
        yield put({type: FETCH_MESSAGE_FAILURE})
    }
}

function* watchFetchMessage () {
    yield takeEvery(FETCH_MESSAGE, FetchMessage);
}

export function* AddMessage(action: MessageAction) {
    try {
        yield create(action.payload!.message!); 
        yield put({type: ADD_MESSAGE_SUCCESS, payload: { message: action.payload!.message!}})
    } catch (error) {
        // TODO: show error
        yield put({type: ADD_MESSAGE_FAILURE})
    }
}

function* watchAddMessage () {
    yield takeEvery(ADD_MESSAGE, AddMessage);
}

export function* UpdateMessage(action: MessageAction) {
    try {
        yield update(action.payload!.message!);
        yield put({type: UPDATE_MESSAGE_SUCCESS, payload: { message: action.payload!.message!}})
    } catch (error) {
        // TODO: show error
        yield put({type: UPDATE_MESSAGE_FAILURE})
    }
}

function* watchUpdateMessage () {
    yield takeEvery(UPDATE_MESSAGE, UpdateMessage);
}

export function* DeleteMessage(action: MessageAction) {
    try {
        yield deleteMess(action.payload!.message!.id);
        yield put({type: DELETE_MESSAGE_SUCCESS, payload: { message: action.payload!.message!}})
    } catch (error) {
        // TODO: show error
        yield put({type: DELETE_MESSAGE_FAILURE})
    }
}

function* watchDeleteMessage () {
    yield takeEvery(DELETE_MESSAGE, DeleteMessage);
}

export function* messageSagas() {
    yield all([
        watchLoadMessages(),
        watchFetchMessage(),
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage() 
    ])
} 