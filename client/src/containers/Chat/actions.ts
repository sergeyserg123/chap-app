import * as actionTypes from "./actionTypes";
import { Message } from "../../models/messages/message";
import { MessageAction } from "./types/messageAction";

export const loadMessages = (): MessageAction => ({
    type: actionTypes.LOAD_MESSAGES
})

export const fetchMessage = (id: string): MessageAction => ({
    type: actionTypes.FETCH_MESSAGE,
    payload: {
        editableMessageId: id
    }
})

export const addMessage = (message: Message): MessageAction => ({
    type: actionTypes.ADD_MESSAGE,
    payload: {
        message
    }  
})

export const updateMessage = (message: Message): MessageAction => ({
    type: actionTypes.UPDATE_MESSAGE,
    payload: {
        message
    }
})

export const deleteMessage = (message: Message): MessageAction => ({
    type: actionTypes.DELETE_MESSAGE,
    payload: {
        message
    }
})