import * as actionTypes from './actionTypes';
import { Message } from '../../models/messages/message';
import { MessageState } from './types/messageState';
import { MessageAction } from './types/messageAction';

const initialState: MessageState = {
    messages: null,
    editableMessage: null
}

const getFilteredMessages = (messages: Message[], deleteId: string) => messages.filter(m => m.id !== deleteId)
const getUpdatedMessages = (messages: Message[], updatedMessage: Message) => messages!.map(m => m.id === updatedMessage.id ? updatedMessage : m)

export const messageReducer = (state: MessageState = initialState, action: MessageAction): MessageState => {
    switch (action.type) {
        case actionTypes.LOAD_MESSAGES_SUCCESS:
            return {
                ...state,
                messages: [...action.payload!.messages!]
            }

        case actionTypes.FETCH_MESSAGE_SUCCESS:
            return {
                ...state,
                editableMessage: action.payload!.message!
            }
        
        case actionTypes.ADD_MESSAGE_SUCCESS:
            return {
                ...state,
                messages: [...state.messages!, action.payload!.message!]
            }

        case actionTypes.UPDATE_MESSAGE_SUCCESS:
            const updatedMessage = action.payload!.message!
            return {
                ...state,
                messages: getUpdatedMessages(state.messages!, updatedMessage)
            }

        case actionTypes.DELETE_MESSAGE_SUCCESS: 
            const { id } = action.payload?.message!
            return {
                ...state,
                messages: getFilteredMessages(state.messages!, id)
            }
    }
    return state
}