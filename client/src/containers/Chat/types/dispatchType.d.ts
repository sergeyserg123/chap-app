import { MessageAction } from './messageAction'

type DispatchType = (args: MessageAction) => MessageAction