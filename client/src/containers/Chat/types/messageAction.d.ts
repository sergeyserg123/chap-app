import { Message } from "../../../models/messages/message"

type MessageAction = {
    type: string
    payload?: {
        messages?: Message[]
        message?: Message,
        editableMessage?: Message
        editableMessageId?: string
    }
}