import { Message } from '../../../models/messages/message'

type MessageState = {
    messages: Message[] | null,
    editableMessage: Message | null
}