import { MessageAction } from '../../store/storeTypes';
import { Message } from '../models/messages/message';
import { User } from '../models/user';
import { DispatchType, RootState } from '../store/storeTypes';
import { History, LocationState } from "history";
import { UserData } from '../../models/users/userData';
import { MessageEditorAction } from '../MessageEditor/MessageEditor';

type PropTypes = {
    messages?: Message[] | null
    history:  History<LocationState>
    userData?: UserData
    isAuthorized: boolean
    loadMessages: () => MessageAction
    addMessage: (message: Message) => MessageAction
    updateMessage: (message: Message) => MessageAction
    deleteMessage: (message: Message) => MessageAction
}

type StateTypes = {
    messages: Message[] | null
    isLoading: boolean
    ownAccounUserData: User | null
}

type ChatActions = {
    loadMessages: () => MessageAction
    addMessage: (message: Message) => MessageAction  
    deleteMessage: (message: Message) => MessageAction
}

declare class Chat {
    state: Readonly<StateTypes>
    createNewMessage: (messageText: string) => void
    likeMessage: (message: Message) => void
    setEditableMessage: (id: string) => void
    editMessage: (message: Message) => void
    deleteMessage: (id: string) => void
    countParticipants: () => number
    countMessages: () => number | undefined
    getMessageTime: (id: string) => string
    getLastMessageTime: () => string | undefined
}