import moment from 'moment';

export const getDate = (): string => {
    return moment().format();
};

//TODO: update to days format
export const formatDate = (date?: string, format: string = 'HH.mm'): string => {
    return moment(date).format(format);
}