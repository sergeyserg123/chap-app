import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./containers/Routing";

export class App extends React.Component {
    render () {
        return (
            <Router>
                <Routing />
            </Router>
        )
    }
}