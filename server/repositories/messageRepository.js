const { BaseRepository } = require('./baseRepository');

class MessageRepository extends BaseRepository {
    constructor() {
        super('messages');
    }

    getById(id) {
        return this.dbContext.find({ id }).value();
    }
}

exports.MessageRepository = new MessageRepository();