const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    getAllMessages() {
        const messages = MessageRepository.getAll();
        return messages;
    }

    getMessageById(id) {
        const message = MessageRepository.getById(id);
        return message;
    }

    createMessage(message) {
        MessageRepository.create(message)
    }

    updateMessage(id, message) {
        MessageRepository.update(id, message)
    }

    deleteMessage(id) {
        MessageRepository.delete(id)
    }
}

module.exports = new MessageService();
