const { throwCustomError } = require('../helpers/throwCustomError');
const { UserRepository } = require('../repositories/userRepository');
const statusCodes = require('../constants/statusCodes');

class UserService {
    getUsers() {
        const users = UserRepository.getAll();
        return users;
    }
    
    getUser(id) {
        if (id == null) {
            throwCustomError(`You aren't logged in.`, statusCodes.BAD_REQUEST);
        }
        const user = UserRepository.getUserById(id);
        return user;
    }

    createUser(userData) {
        const emailUser = UserRepository.getUserByEmail(userData.email);

        // TODO: simple method
        if (emailUser) {
            throwCustomError(`User with email ${userData.email} is already exist.`, statusCodes.BAD_REQUEST);
        }
        const user = UserRepository.create(userData);    
        return user;
    } 

    updateUser(id, user) {
        const oldUser = UserRepository.getUserById(id);
       
        if(!oldUser) {
            throwCustomError(`User not found.`, statusCodes.NOT_FOUND);
        }

        const updatedUser = UserRepository.update(id, user);
       
        if(!updatedUser) {
            throwCustomError(`Could not update the user.`, statusCodes.BAD_REQUEST);
        }
       
        return updatedUser;     
    }

    deleteUserById(id) {
        const oldUser = UserRepository.getUserById(id);
        
        if(!oldUser) {
            throwCustomError(`User not found.`, statusCodes.NOT_FOUND);
        }
        
        const deletedUser = UserRepository.delete(id);
        
        if(!deletedUser) {
            throwCustomError(`Could not delete the user.`, statusCodes.BAD_REQUEST);
        }
        
        return deletedUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();