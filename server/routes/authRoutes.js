const { Router } = require('express');
const AuthService = require('../services/authService');

const router = Router();

router.post('/login', (req, res, next) => {
    const user = AuthService.login(req.body);
    res.send(user);
});

router.post('/register', (req, res, next) => {
    const user = AuthService.singUp(req.body);
    res.send(user);
});

module.exports = router;