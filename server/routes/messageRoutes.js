const { Router } = require('express');
const MessageService = require('../services/messageService');

const router = Router();

router.get('', (req, res, next) => {
    const messages = MessageService.getAllMessages();
    res.send(messages);
});

router.get('/:id', (req, res, next) => {
    const { id } = req.params
    const message = MessageService.getMessageById(id);
    res.send(message);
});

router.post('', (req, res, next) => {
    MessageService.createMessage(req.body);
    res.send();
});

router.put('/:id', (req, res, next) => {
    const { id } = req.params
    const message = req.body
    MessageService.updateMessage(id, message);
    res.send();
});

router.delete('/:id', (req, res, next) => {
    MessageService.deleteMessage(req.params.id);
    res.send();
});

module.exports = router;